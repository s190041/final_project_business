package com.example.demo.rest.resources;


import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import com.example.demo.system.Token;


@Path("/hello")
public class HelloEndpoint {
    @GET
    @Produces("text/plain")
    public String doGet() {
        return "Hello from Thorntail!";
    }
    
//    @GET
//    @Path("/requestToken")
//    public ArrayList<Token> requestToken() {
//        
//    }
}
