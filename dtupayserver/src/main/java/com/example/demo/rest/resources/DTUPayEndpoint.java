package com.example.demo.rest.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import com.example.demo.system.*;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import com.example.demo.rest.dto.*;


/* Note that the resource names and the exclusive use of HTTP POST is on purpose.
 * You should be designing the right resource URI's and use the correct HTTP verb yourself.
 */
@Path("/dtupay")
public class DTUPayEndpoint {
	
	private static DTUPay dtuPay = new DTUPay(new BankServiceService().getBankServicePort());
	
	
	@GET
	@Path("test")
	@Produces("text/plain")
	public String test() {
		
		return "yo yo yo";
	}
	
	@POST
	@Path("customers")
	@Consumes("application/json")
	@Produces("text/plain")
	public String registerCustomer(DtuPayUserRepresentation c) {
		dtuPay.registerCustomer(c.getCpr()
				, c.getFirstName()
				, c.getLastName()
				, c.getAccount());
		return c.getCpr();
	}
	
	
	@POST
	@Path("merchants")
	@Consumes("application/json")
	@Produces("text/plain")
	public String registerMerchant(DtuPayUserRepresentation m) {
		dtuPay.registerMerchant(m.getCpr()
				, m.getFirstName()
				, m.getLastName()
				, m.getAccount());
		return m.getCpr();
	}
	
	@POST
	@Path("tokenRequest")
	@Consumes("application/json")
	@Produces("application/json")
	public List<Token> requestTokens(TokenRequest r) {
		return dtuPay.requestTokens(r.getCpr(), r.getNumber());
	}
	
/*	@POST
	@Path("MarkToken")
	@Consumes("application/json")
	public void markTokenUsed(Token token) {
		dtuPay.markTokenUsed(token);
	} */
	
	
/*	@POST
	@Path("GetTokenStatus")
	@Consumes("application/json")
	@Produces("text/plain")
	public boolean getTokenStatus(Token token) {
		return dtuPay.getTokenStatus(token);  
	} */
	
	@POST
	@Path("pay")
	@Consumes("application/json")
	@Produces("text/plain")
	public Response payWithToken(PaymentRequest p) throws BankServiceException_Exception {
		return dtuPay.payWithToken(p.getToken()
				, p.getCustomerAcct()
				, p.getMerchantAcct()	
				, p.getAmount());
	}
}