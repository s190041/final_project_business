package com.example.demo.system;

/**
 * 
 * @author Anelia
 *
 */
public abstract class DtuPayUser {

    private String firstName;
    private String lastName;
    private String cpr;
    
    public DtuPayUser(String cpr, String firstName, String lastName) {
        this.cpr = cpr;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    public String getCpr() {
        return cpr;
    }
}
