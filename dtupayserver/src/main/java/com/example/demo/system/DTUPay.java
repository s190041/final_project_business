package com.example.demo.system;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;


import com.example.demo.rest.dto.*;


public class DTUPay {
    
    BankService bank;
    WebTarget userManagerUrl, tokenManagerUrl,paymentManagerUrl;
    
    List<Token> tokenList = new ArrayList<>();
    HashMap<String, Customer> customerList = new HashMap<>();
    HashMap<String, Merchant> merchantList = new HashMap<>();
    HashMap<String, List<Token>> cprToTokens = new HashMap<>();
    
    private String tokenServicePath = "TokenService";
    
    
    public DTUPay(BankService bankService) {
        this.bank = bankService;
        Client client = ClientBuilder.newClient();
        Client client2 = ClientBuilder.newClient();
        Client client3 = ClientBuilder.newClient();
        userManagerUrl = client.target("http://fastmoney-23.compute.dtu.dk:8085/");
        tokenManagerUrl = client2.target("http://fastmoney-23.compute.dtu.dk:8080/");
        paymentManagerUrl = client3.target("http://fastmoney-23.compute.dtu.dk:8881/");
    }
    
    public void registerCustomer(String customerCPR, String firstName, String lastName, String account) {
//        Customer customer = new Customer(customerCPR, firstName, lastName);
//        customerList.put(customerCPR, customer);
//        Token token = new Token();
//        tokenList.add(token);
//        cprToTokens.put(customerCPR, tokenList);
    	
		DtuPayUserRepresentation customer = new DtuPayUserRepresentation();
		customer.setCpr(customerCPR);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setAccount(account);
		userManagerUrl.path("/")
		.path("customers")
		.request()
		.post(Entity.entity(customer, "application/json"));    	
    }
    
    public void registerMerchant(String merchantCPR, String firstName, String lastName, String account) {
		DtuPayUserRepresentation merchant = new DtuPayUserRepresentation();
		merchant.setCpr(merchantCPR);
		merchant.setFirstName(firstName);
		merchant.setLastName(lastName);
		merchant.setAccount(account);
		userManagerUrl.path("/")
		.path("merchants")
		.request()
		.post(Entity.entity(merchant, "application/json"));
    }
    
	public void markTokenUsed(Token token) {		
		tokenManagerUrl.path(tokenServicePath)
		.path(String.format("tokens/%s", token.getTokenId()))
		.request()
		.post(Entity.entity(token.getTokenId(), "application/json"));
		
	}
	
	public List<Token> requestTokens(String cprNumber, int i) {		
		TokenRequest tr = new TokenRequest();
		tr.setCpr(cprNumber);
		tr.setNumber(i);
		
//		Token[] tokens = userManagerUrl.path(tokenServicePath)
//		.path("tokens")
//		.request()
//		.post(Entity.entity(tr, "application/json"), Token[].class);
		
		Token[] tokens = tokenManagerUrl.path(tokenServicePath)
    		.path("tokens")
    		.request()
    		.accept(MediaType.APPLICATION_JSON)
    		.post(Entity.entity(tr, "application/json"), Token[].class);
		
		return Arrays.asList(tokens);
	}

	public boolean getTokenStatus(Token token) {        
		boolean result =
				tokenManagerUrl.path(tokenServicePath)
		    		.path(String.format("tokens/%s", token.getTokenId()))
		    		.request()
		    		.accept(MediaType.TEXT_PLAIN)
		    		.get(Boolean.class);
		        
		        return result;  
	}

    public Response requestPayment(String account1, String account2, BigDecimal i, String comment) {
   	     PaymentRequest pr = new PaymentRequest(account1,account2,i, comment);
   	     Response response = paymentManagerUrl.path("payment").request().post(Entity.json(pr),Response.class);
   	     return response;
   	}
        
    public Response payWithToken(Token token, String customerAccount, String merchantAccount, BigDecimal amount) {
//        String description = String.format("DTUPay transaction with token %s", token.getTokenId());
    	String description = "PaymentRequested";
        if(!getTokenStatus(token)) {
		Response response = requestPayment(customerAccount, merchantAccount, amount, description);
		markTokenUsed(token);
		return response;
		}
		else return null;
    }
    
 /*   public boolean payWithToken(Token token, String customerAccount, String merchantAccount, BigDecimal amount) throws BankServiceException_Exception {
    if(!getTokenStatus(token)) {
    	bank.transferMoneyFromTo(customerAccount, merchantAccount, amount, "comment");
    	return true;
    }
    else return false;
}*/
}
