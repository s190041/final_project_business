package com.example.demo.system;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IDTUPay {

    @WebMethod
    public List<Token> requestToken();
    @WebMethod
    public void registerCustomer(String cpr, String firstName, String lastName);
    @WebMethod
    public void registerMerchant(String cpr, String firstName, String lastName);
}
