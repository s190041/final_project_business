package com.example.demo.rest.dto;

import com.example.demo.system.*;
//import dtupay.main.Token;
import java.math.BigDecimal;

public class PaymentRequest {

	private Token token;
	public String debtor;
	public String creditor;
	public BigDecimal amount;
	public String comment;

	public PaymentRequest() {};
	
	public PaymentRequest(String account1, String account2, BigDecimal i,String comment) {
		debtor = account1;
		creditor = account2;
		amount = i;
		this.comment = comment;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getMerchantAcct() {
		return creditor;
	}
	public String getCustomerAcct() {
		return debtor;
	}
	
	public void setMerchantAcct(String creditor) {
		this.creditor = creditor;
	}
	
	public void setCustomerAcct(String debtor) {
		this.debtor = debtor;
	}
	public Token getToken() {
		return token;
	}
	public void setToken(Token token) {
		this.token = token;
	}

	}

