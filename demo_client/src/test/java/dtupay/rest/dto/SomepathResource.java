package dtupay.rest.dto;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import helloservice.DTUPay;

/* Note that the resource names and the exclusive use of HTTP POST is on purpose.
 * You should be designing the right resource URI's and use the correct HTTP verb yourself.
 */
@Path("/dtupay")
public class SomepathResource {
	
	private static DTUPay dtuPay = new DTUPay(new BankServiceService().getBankServicePort());
	
	
	@GET
	@Path("test")
	@Produces("text/plain")
	public String test() {
		
		return "yo yo yo";
	}
	
	@POST
	@Path("customers")
	@Consumes("application/json")
	@Produces("text/plain")
	public String registerCustomer(DtuPayUserRepresentation c) {
		dtuPay.registerCustomer(c.getCpr()
				, c.getFirstName()
				, c.getLastName()
				, c.getAccount());
		return c.getCpr();
	}
	
	
	@POST
	@Path("merchants")
	@Consumes("application/json")
	@Produces("text/plain")
	public String registerMerchant(DtuPayUserRepresentation m) {
		dtuPay.registerMerchant(m.getCpr()
				, m.getFirstName()
				, m.getLastName()
				, m.getAccount());
		return m.getCpr();
	}
	
	@POST
	@Path("tokens")
	@Consumes("application/json")
	@Produces("application/json")
	public List<Token> requestTokens(TokenRequest r) {
		return dtuPay.requestTokens(r.getCpr(), r.getNumber());
	}

	@POST
	@Path("pay")
	@Consumes("application/json")
	@Produces("text/plain")
	public boolean pay(PaymentRequest p) throws BankServiceException_Exception {
		return dtuPay.payWithToken(p.getToken()
				, p.getCustomerAcct()
				, p.getMerchantAcct()	
				, p.getAmount());
	}
}